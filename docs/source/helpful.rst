Helpful commands
================

GIT
---

Show a dense list of all git commits:

..  code::

    git log --pretty=format:"%h: %s"
    367b8d9: once more, more data
    839ae8c: all data removed
    7658809: another data generation run
    b8b48d1: now tracking data/
    95b4a9b: stop tracking data
    f17eee6: first set of data generated
    9567f39: added DVC S3 config
    be5941d: Initialized DVC
    6a9c5d7: Initial code commit
