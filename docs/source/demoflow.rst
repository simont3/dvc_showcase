Demo flow
=========

Preparation
-----------

*   Show the HCP Namespace/Bucket to be used as DVC data store (*maybe*
    *even show the Namespace creation and setup*)

*   Have the ``showcase`` script ready in an otherwise clean project folder

*   Init the git repo in the project folder

*   Init DVC

*   configure the HCP Namespace/Bucket as a target for DVC-S3

*   Commit the status to GIT with an initial commit

Initial data generation
-----------------------

*   Run the data generation script once,
    and show the data being created

*   Add the data to DVC

*   Have DVC push the data to HCP
    (*show that the data has been written to HCP*)

*   Commit the changes to GIT

Further data generation
-----------------------

*   do the same thing as before a few times

*   sometime, delete all existing data,
    ``dvc add`` the change and commit to git

*   some more data generation cycles

Show the beauty
---------------

*   move back and forth between the commits

    *   ``git check out <version>``
    *   ``dvc checkout``

    and show how the data folder looks like