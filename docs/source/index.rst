.. showcase documentation master file, created by
   sphinx-quickstart on Tue Oct  3 14:51:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DVC showcase
============

.. toctree::
   :maxdepth: 4

   demoflow
   dvc-config
   helpful