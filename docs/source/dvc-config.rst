HowTo set up and use DVC
========================

Preparation
-----------

*   Clone the dvc_showcase repo

    *   ``git clone https://gitlab.com/simont3/dvc_showcase --depth=1``

*   ``cd`` into the project folder

    *   ``cd ~/dvc_showcase``

*   create a Python virtual environment to work with:

    *   ``python3.11 -m venv .venv``
    *   ``source .venv/bin/activate``
    *   ``pip install -U pip setuptools wheel``

*   Install DVC and it's S3 add-on

    *   ``pip install -r pip-requirements.txt``

*   Init DVC

    *   ``dvc init``

*   Add necessary files to be added and do a first git commit

    *   ``git add .dvc/.gitignore .dvc/config .dvcignore``
    *   ``git commit -m "Initialize DVC"``

*   Setup DVC connection to HCP

    *   S3 store:

            | endpoint: dvc.ed-ts-hcp164.tslab.coe.net
            | bucket: showcase
            | access key: ZHZj (user dvc)
            | secret key: 0c47dbfb495d27ee9fd2eb9df2ba59c0  (password=Password*****)

    *   Edit the script provided in the ``tools`` folder to you needs and run it to setup
        DVCs connection to HCP: ``bash tools/mk_dvc_s3.sh``

    *   add/commit the config to GIT:

        *   ``git add .dvc/config``
        *   ``git commit -m "added dvc-s3 config"``


Working with data
-----------------

Initial data generation
+++++++++++++++++++++++

*   Generate some date using our code sample

    ``python showcase.py``

    | generating sample data (3 x 100.00 MiB):
    |     generating file 1 as ../data/2023-10-03/12:42/8cb62fe461d911ee8183005056a7e82e
    |     generating file 2 as ../data/2023-10-03/12:42/8ce9ee7e61d911ee8183005056a7e82e
    |     generating file 3 as ../data/2023-10-03/12:42/8d1d24e261d911ee8183005056a7e82e
    | done

*   add the ``data`` folder to be managed by DVC

    ``dvc add data/``

*   add the changed files to git

    ``git add .gitignore data/``

*   commit to git

    ``git commit -m "first data generation"``

*   push the data to HCP

    | ``dvc push``
    | 4 files pushed

Further data generation
+++++++++++++++++++++++

*   run the sequence of

    | ``python showcase.py``
    | ``dvc add data/``
    | ``git add data.dvc``
    | ``git commit -m "further data generation"``
    | ``dvc push``

    several times

*   next, let's delete all the data we had so far

    | ``rm -rf data/*``
    | ``dvc add data/``
    | ``git add data.dvc``
    | ``git commit -m "all generated data removed"``

*   and more data generation

    | ``python showcase.py``
    | ``dvc add data/``
    | ``git add data.dvc``
    | ``git commit -m "further data generation"``
    | ``dvc push``

