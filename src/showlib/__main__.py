from pathlib import Path
from datetime import datetime
from uuid import uuid1

from .calc import calcbytesize
from .gendata import gendatafile

def main():
    now = datetime.now()
    datapath = Path(f'../data/{now.year:04}-{now.month:02}-{now.day:02}/{now.hour:02}:{now.minute:02}')
    datapath.mkdir(parents=True, exist_ok=True)
    filesize = 1024**2*100  # 100 MiB
    numfiles = 3

    print(f'generating sample data ({numfiles} x {calcbytesize(filesize)}):')

    for x in range(1, numfiles+1):
        filename = datapath/uuid1().hex
        print(f'\tgenerating file {x} as {filename}')
        gendatafile(size=filesize, filename=filename)

    print('done')
