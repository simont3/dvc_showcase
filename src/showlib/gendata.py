from os import urandom

def gendatafile(size: int=1024**2, chunksize: int=1024**2, filename: str='') -> None:
    """
    Generates a data file with random binary content.

    :param size:       the number of bytes wanted
    :param chunksize:  the number of bytes read from /dev/urandom at once
    :param filename:   the name of the file to write to
    """
    if size < chunksize:
        raise AttributeError('size can\'t be smaller than chunksize')
    if size < 1 or chunksize < 1:
        raise AttributeError('neither size nor chunksize can be zero or negative')

    byteswritten = 0
    with open(filename, 'wb') as ohdl:
        while byteswritten < size:
            ohdl.write(urandom(chunksize))
            byteswritten += chunksize
