# this script sets up DVC to use an HCP Bucket as a remote storage target
# you need to edit it to match your environment

# Settings
PROJECTNAME='dvc_showcase'
S3_endpointurl='https://dvc.ed-ts-hcp164.tslab.coe.net:443'
S3_bucket='dvc_showcase'
S3_region='us-east-2'
S3_verify='false'
S3_access_key='ZHZj'
S3_secret_key='0c47dbfb495d27ee9fd2eb9df2ba59c0'

# action
dvc remote add --project $PROJECTNAME s3://$S3_Bucket
dvc remote modify --project $PROJECTNAME endpointurl $S3_endpointurl
dvc remote modify --project $PROJECTNAME region $S3_region
dvc remote modify --project $PROJECTNAME ssl_verify $S3_verify
dvc remote modify --project $PROJECTNAME access_key_id $S3_access_key
dvc remote modify --project $PROJECTNAME secret_access_key $S3_secret_key
dvc config --project core.remote $PROJECTNAME
